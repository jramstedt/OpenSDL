#  Introduction

The OpenSDL application requires a number of additional packages to be installed
before being able to successfully build it.

**Note**: These build instructions were generated when building on
Ubuntu-Budgie 19.10.  The items listed here should be consistent with versions
prior and following the indicated Ubuntu release.  Building on other Linux
releases or Cygwin may be require different packages.  Your mileage may vary.

## Prerequisites

The following packages are required to be able to build DECaxp successfully.

 # cmake
 # bison
 # flex
 # texlive
 # texlive-extra-utils
 # texlive-plain-generic
 # texinfo

```{.bash}
sudo apt install cmake bison flex texlive texlive-extra-utils texlive-plain-generic texinfo
```

**Note**: The prerequisite packages may change as development continues and
prior to this file getting updated.

## Running the build
 
The `build.sh` file contains the steps required to build DECaxp.  This script
contains help text to allow you to instruct it how and what to build.
 
```{.bash}
$ ./build.sh -h
Usage: ./build.sh [-h|--help]|[-d|--debug] [-r|--refresh]
  -d|--debug      instruct cmake to build for debug
  -h|--help       display this help text
  -r|--refresh    build from scratch
$ ./build.sh -h
```

The generated executable will be put in the `<project-directory>/build/opensdl/`
directory.

A successful build should generate the following output:

```{.bash}
$  ./build.sh -r
-- The C compiler identification is GNU 9.2.1
-- The CXX compiler identification is GNU 9.2.1
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found BISON: /usr/bin/bison (found version "3.4.1") 
-- Found FLEX: /usr/bin/flex (found version "2.6.4") 
-- Configuring done
-- Generating done
-- Build files have been written to: /home/belanger/projects/OpenSDL/build
Scanning dependencies of target OpenSDL_common
[  5%] Building C object library/common/CMakeFiles/OpenSDL_common.dir/opensdl_blocks.c.o
[ 10%] Building C object library/common/CMakeFiles/OpenSDL_common.dir/opensdl_message.c.o
[ 15%] Linking C static library libOpenSDL_common.a
[ 15%] Built target OpenSDL_common
[ 20%] Generating opensdl_lexical.c
/usr/bin/flex version 2.6.4 usage statistics:
  scanner options: -i--reentrant--bison-bridge--bison-locationsvI8 -Cem -oopensdl_lexical.c
  4329/5000 NFA states
  2794/3000 DFA states (23621 words)
  144 rules
  Compressed tables always back-up
  9/40 start conditions
  2571 epsilon states, 1725 double epsilon states
  31/100 character classes needed 593/750 words of storage, 0 reused
  186655 state/nextstate pairs created
  9529/177126 unique/duplicate transitions
  2881/3000 base-def entries created
  11210/12000 (peak 17844) nxt-chk entries created
  783/7500 (peak 7308) template nxt-chk entries created
  365 empty table entries
  138 protos created
  87 templates created, 1618 uses
  84/256 equivalence classes created
  9/256 meta-equivalence classes created
  333 (340 saved) hash collisions, 6754 DFAs equal
  14 sets of reallocations needed
  28522 total table entries needed
[ 25%] Generating opensdl_parser.c
Scanning dependencies of target OpenSDL_lexical
[ 30%] Building C object library/parser/CMakeFiles/OpenSDL_lexical.dir/opensdl_parser.c.o
[ 35%] Building C object library/parser/CMakeFiles/OpenSDL_lexical.dir/opensdl_lexical.c.o
[ 40%] Linking C static library libOpenSDL_lexical.a
[ 40%] Built target OpenSDL_lexical
Scanning dependencies of target OpenSDL_utility
[ 45%] Building C object library/utility/CMakeFiles/OpenSDL_utility.dir/opensdl_actions.c.o
[ 50%] Building C object library/utility/CMakeFiles/OpenSDL_utility.dir/opensdl_listing.c.o
[ 55%] Building C object library/utility/CMakeFiles/OpenSDL_utility.dir/opensdl_plugin.c.o
[ 60%] Building C object library/utility/CMakeFiles/OpenSDL_utility.dir/opensdl_utility.c.o
[ 65%] Linking C static library libOpenSDL_utility.a
[ 65%] Built target OpenSDL_utility
Scanning dependencies of target OpenSDL_c
[ 70%] Building C object library/language/CMakeFiles/OpenSDL_c.dir/opensdl_c.c.o
[ 75%] Linking C shared library libOpenSDL_c.so
[ 75%] Built target OpenSDL_c
Scanning dependencies of target OpenSDL
[ 80%] Building C object opensdl/CMakeFiles/OpenSDL.dir/opensdl_main.c.o
[ 85%] Linking C executable OpenSDL
[ 85%] Built target OpenSDL
Scanning dependencies of target struct_test
[ 90%] Building C object test/CMakeFiles/struct_test.dir/struct_test.c.o
[ 95%] Linking C executable struct_test
[ 95%] Built target struct_test
Scanning dependencies of target documentation
[100%] Creating PDF file /home/belanger/projects/OpenSDL/build/documentation/opensdl.pdf
This is pdfTeX, Version 3.14159265-2.6-1.40.20 (TeX Live 2019/Debian) (preloaded format=pdfetex)
 restricted \write18 enabled.
entering extended mode
(/home/belanger/projects/OpenSDL/documentation/opensdl.texi
(/usr/share/texmf/tex/texinfo/texinfo.tex
Loading texinfo [version 2019-02-16.14]: pdf, fonts, markup, glyphs,
page headings, tables, conditionals, indexing, sectioning, toc, environments,
defuns, macros, cross references, insertions,
(/usr/share/texlive/texmf-dist/tex/generic/epsf/epsf.tex
This is `epsf.tex' v2.7.4 <14 February 2011>
) localization, formatting, and turning on texinfo input format.)
(/home/belanger/projects/OpenSDL/documentation/version.texi) [1{/var/lib/texmf/
    .
    .   (truncated output)
    .
Output written on opensdl.pdf (124 pages, 1285389 bytes).
Transcript written on opensdl.log.
[100%] Built target documentation
```

**Note**: As development continues, the above output will change.  The above is the output
generated at the time of the initial writing of this document.
